const Telegraf = require('telegraf');
const { Configuration, OpenAIApi } = require('openai');

const bot = new Telegraf.Telegraf(process.env.TELEGRAM_TOKEN)
const configuration = new Configuration(
    { apiKey: process.env.OPENAI_API_KEY }
);

const openai = new OpenAIApi(configuration);

bot.on("text", async (ctx) => {
    const chatResponse = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: [{ role: "user", content: ctx.message.text }],
    });
    ctx.reply(chatResponse.data.choices[0].message.content);
})

bot.launch()